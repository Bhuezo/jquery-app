//Variables para la posicion en las vistas
var position = 0;
//Variable para la posicion en las configuraciones
var conf = 0;
//Variable para borrar
var tras = false
var edit = false
$('#config').css("display", "none")
$(document).ready(function(){
        $('.imagen').attr('src','./../img/2.png')
    $('.borrar').css('display','none')
    $('.editar').css('display','none')
    $('select').formSelect();
    $('.fixed-action-btn').floatingActionButton({
        direction: 'top',
        hoverEnabled: false
    });
    $('.datepicker').datepicker();
    $('.timepicker').timepicker();
    $('.modal').modal();
})
//Botones de navegacion
$("#btn1").on('click', function(){
    $('#conten').removeClass('activo3');
    $('#conten').removeClass('activo2');
    $('#conten').removeClass('activo4');
    
    $('#conten').addClass('activo1');
    position = 0;
    $('#btn3').css("color", "#4b4b4b")
    $('#btn2').css("color", "#4b4b4b")
    $('#btn1').css("color", "#28c2b2")
    
    $('#btn1').css("font-size", "20px")
    $('#btn2').css("font-size", "13px")
    $('#btn3').css("font-size", "13px")
    $('#float').css("display", "")
    conf = 0;
    
})
$("#btn2").on('click', function(){
    $('#conten').removeClass('activo1');
    $('#conten').removeClass('activo3');
    $('#conten').removeClass('activo4');
    $('#conten').addClass('activo2');
    $('#btn1').css("color", "#4b4b4b")
    $('#btn2').css("color", "#28c2b2")
    $('#btn3').css("color", "#4b4b4b")
    
    $('#btn1').css("font-size", "13px")
    $('#btn2').css("font-size", "20px")
    $('#btn3').css("font-size", "13px")
    position = 1;
    $('#float').css("display", "none")
    conf = 0;
    
})
$("#btn3").on('click', function(){
    $('#conten').removeClass('activo1');
    $('#conten').removeClass('activo2');
    $('#conten').removeClass('activo4');
    
    $('#conten').addClass('activo3');
    $('#btn1').css("color", "#4b4b4b")
    $('#btn2').css("color", "#4b4b4b")
    $('#btn3').css("color", "#28c2b2")

    
    $('#btn1').css("font-size", "13px")
    $('#btn2').css("font-size", "13px")
    $('#btn3').css("font-size", "20px")
    position = 2;
    $('#float').css("display", "none")
    
})
//Botones de configuraciones
$("#general").on('click', function(){
    $('#conten').removeClass('activo3');
    $('#conten').addClass('activo4');
    position = 3;
    conf = 1;
    $('#config').css("display", "none")
    $('#info').css("display", "")
});
$("#configu").on('click', function(){
    $('#conten').removeClass('activo3');
    $('#conten').addClass('activo4');
    position = 3;
    conf = 2;
    $('#config').css("display", "")
    $('#info').css("display", "none")
    
});
$('#trash').on('click', function(){
    $('.editar').css('display','none')
    $('#edit2').addClass('fa-pen');
    $('#edit2').removeClass('fa-times');
    if (!tras) {
        $('.borrar').css('display','')
        $('#trash2').removeClass('fa-trash');
        $('#trash2').addClass('fa-times');
        tras = true
        edit = false
    }
    else {
        $('.borrar').css('display','none')
        $('#trash2').addClass('fa-trash');
        $('#trash2').removeClass('fa-times');
        tras = false
    }
})
$('#edit').on('click', function(){
    $('.borrar').css('display','none')
    $('#trash2').addClass('fa-trash');
    $('#trash2').removeClass('fa-times');
    if (!edit) {
        $('#edit2').removeClass('fa-pen');
        $('#edit2').addClass('fa-times');
        $('.editar').css('display','')
        edit = true
        tras = false
    }
    else {
        $('.editar').css('display','none')
        $('#edit2').addClass('fa-pen');
        $('#edit2').removeClass('fa-times');
        edit = false
    }
})
//Configuracion de swipe para esilo movile
document.addEventListener('touchstart', handleTouchStart, false);
document.addEventListener('touchmove', handleTouchMove, false);
var xDown = null
var yDown = null;
function handleTouchStart (evt){
    xDown = evt.touches[0].clientX;
    yDown = evt.touches[0].clientY;
}
function handleTouchMove (evt){
    if (!xDown || !yDown ) {
        return;
    }
    var xUp = evt.touches[0].clientX
    var yUp = evt.touches[0].clientY
    var xDiff = xDown - xUp
    var yDiff = yDown - yUp
    if (Math.abs(xDiff) > Math.abs(yDiff)) {
        if (xDiff > 10) {
            //A la izquierda pero cambia a la derecha
            switch (position) {
                case 0:
                    $('#conten').removeClass('activo1');
                    $('#conten').removeClass('activo3');
                    $('#conten').removeClass('activo4');
                    
                    $('#conten').addClass('activo2');
                    $('#btn1').css("color", "#4b4b4b")
                    $('#btn2').css("color", "#28c2b2")
                    $('#btn3').css("color", "#4b4b4b")
                    $('#float').css("display", "none")

                                    
                    $('#btn1').css("font-size", "13px")
                    $('#btn2').css("font-size", "20px")
                    $('#btn3').css("font-size", "13px")
                    position = 1
                    conf = 0;
                break;
                case 1:
                    $('#conten').removeClass('activo1');
                    $('#conten').removeClass('activo2');
                    $('#conten').removeClass('activo4');
                    
                    $('#conten').addClass('activo3');
                    $('#btn1').css("color", "#4b4b4b")
                    $('#btn2').css("color", "#4b4b4b")
                    $('#btn3').css("color", "#28c2b2")
                    
                    $('#btn1').css("font-size", "13px")
                    $('#btn2').css("font-size", "13px")
                    $('#btn3').css("font-size", "20px")
                    position = 2
                    $('#float').css("display", "none")
                    conf = 0;
                break;
                case 2:
                    if (conf == 1){
                        $('#conten').removeClass('activo3');
                        
                        $('#conten').addClass('activo4');
                        conf = 1;

                        position = 3
                    }
                    else if (conf == 2){
                        $('#conten').removeClass('activo3');
                        
                        $('#conten').addClass('activo5');
                        conf = 2;

                        position = 4
                    }
                break;
            }
        }
        else if (xDiff < -10){
            //A la Derecha pero cambia a al izquierda
            switch (position) {
                case 0:
                break;
                case 1:
                    $('#conten').removeClass('activo3');
                    $('#conten').removeClass('activo2');
                    $('#conten').removeClass('activo4');
                    
                    $('#conten').addClass('activo1');
                    $('#btn3').css("color", "#4b4b4b")
                    $('#btn2').css("color", "#4b4b4b")
                    $('#btn1').css("color", "#28c2b2")

                    $('#btn1').css("font-size", "20px")
                    $('#btn2').css("font-size", "13px")
                    $('#btn3').css("font-size", "13px")
                    position = 0
                    $('#float').css("display", "")
                    conf = 0;
                break;
                case 2:
                    $('#conten').removeClass('activo1');
                    $('#conten').removeClass('activo3');
                    $('#conten').removeClass('activo4');
                    
                    $('#conten').addClass('activo2');
                    $('#btn1').css("color", "#4b4b4b")
                    $('#btn2').css("color", "#28c2b2")
                    $('#btn3').css("color", "#4b4b4b")

                    $('#btn1').css("font-size", "13px")
                    $('#btn2').css("font-size", "20px")
                    $('#btn3').css("font-size", "13px")
                    position = 1
                    $('#float').css("display", "none")
                break;
                case 3:
                    $('#conten').removeClass('activo1');
                    $('#conten').removeClass('activo4');
                    $('#conten').removeClass('activo2');
                    
                    $('#conten').addClass('activo3');
                    position = 2
                    $('#float').css("display", "none")
                    conf = 1;
                break;
                case 4:
                    $('#conten').removeClass('activo1');
                    $('#conten').removeClass('activo4');
                    $('#conten').removeClass('activo2');
                    
                    $('#conten').addClass('activo3');
                    position = 2
                    $('#float').css("display", "none")
                    conf = 2;
                break;
            }
        }
    }
    else {
        if (yDiff < 0 && position == 2) {
            //abajo que se mueve para arriba
        }
        else {
            //Para arriba se mueve para abajo
        }
    }
    xDown = null;
    yDown = null;
}